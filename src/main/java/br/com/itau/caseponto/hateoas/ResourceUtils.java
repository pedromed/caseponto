package br.com.itau.caseponto.hateoas;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

import java.net.URI;

public class ResourceUtils<T extends RepresentationModel<? extends T>> {

    public URI createURI(RepresentationModel<T> created) {
        if (created == null) return URI.create("");
        Link self = created
                .getLink("self")
                .orElse(null);
        if (self == null) return URI.create("");
        return URI.create(self.getHref());
    }
}
