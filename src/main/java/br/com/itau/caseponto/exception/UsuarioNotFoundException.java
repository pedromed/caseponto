package br.com.itau.caseponto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UsuarioNotFoundException extends Exception {
    public UsuarioNotFoundException(Object id) {
        super(String.format("Usuario %s nao encontrado", id));
    }
}
