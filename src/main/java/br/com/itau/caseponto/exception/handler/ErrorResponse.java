package br.com.itau.caseponto.exception.handler;

import java.util.List;

public class ErrorResponse {

    private List<ErrorDetails> errors;

    public ErrorResponse(List<ErrorDetails> errors) {
        this.errors = errors;
    }

    public List<ErrorDetails> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorDetails> errors) {
        this.errors = errors;
    }
}