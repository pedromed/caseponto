package br.com.itau.caseponto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidTipoRegistroException extends Exception {
    public InvalidTipoRegistroException(Object... objects) {
        super(String.format("Tipo de registro %s invalido - Valores aceitos: '%s', '%s'", objects));
    }
}
