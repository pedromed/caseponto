package br.com.itau.caseponto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidNewTipoRegistroException extends Exception {
    public InvalidNewTipoRegistroException(Object... objects) {
        super(String.format("Novo tipo de registro '%s' invalido, deveria ser do tipo: '%s'", objects));
    }
}
