package br.com.itau.caseponto.exception.handler;

import org.springframework.validation.FieldError;

public class ErrorDetails {
    private String fieldName;
    private String message;

    public ErrorDetails(FieldError fieldError) {
        this.fieldName = fieldError.getField();
        this.message = fieldError.getDefaultMessage();
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}