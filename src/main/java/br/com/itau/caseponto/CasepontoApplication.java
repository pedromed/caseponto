package br.com.itau.caseponto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CasepontoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasepontoApplication.class, args);
    }

}
