package br.com.itau.caseponto.controller;

import br.com.itau.caseponto.exception.UsuarioNotFoundException;
import br.com.itau.caseponto.hateoas.ResourceUtils;
import br.com.itau.caseponto.model.Usuario;
import br.com.itau.caseponto.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping("/ponto/v1")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/usuarios")
    public ResponseEntity<Usuario> createUsuario(@Validated(Usuario.UsuarioCreation.class) @RequestBody Usuario newUsuario) {
        Usuario createdUsuario = usuarioService.create(newUsuario);
        createHATEOASLinks(createdUsuario);
        URI uri = new ResourceUtils<Usuario>().createURI(createdUsuario);
        return ResponseEntity.created(uri).body(createdUsuario);
    }

    @GetMapping("/usuarios")
    public List<Usuario> getAllUsuarios() {
        return usuarioService.findAll();
    }

    @GetMapping("/usuarios/{id}")
    public ResponseEntity<Usuario> getUsuarioById(@PathVariable(value = "id") Long usuarioId) throws UsuarioNotFoundException {
        return ResponseEntity.ok().body(usuarioService.findById(usuarioId));
    }

    @PutMapping("/usuarios/{id}")
    public ResponseEntity<Usuario> updateUsuario(@PathVariable(value = "id") Long oldId, @Validated(Usuario.UsuarioUpdate.class) @RequestBody Usuario newUsuario) throws UsuarioNotFoundException {
        return ResponseEntity.ok().body(usuarioService.update(oldId, newUsuario));
    }

    private void createHATEOASLinks(Usuario created) {
        if (created == null) return;
        created.add(linkTo(UsuarioController.class).slash("usuarios").slash(created.getId()).withSelfRel());
    }
}