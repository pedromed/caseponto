package br.com.itau.caseponto.controller;

import br.com.itau.caseponto.exception.InvalidNewTipoRegistroException;
import br.com.itau.caseponto.exception.InvalidTipoRegistroException;
import br.com.itau.caseponto.exception.UsuarioNotFoundException;
import br.com.itau.caseponto.hateoas.ResourceUtils;
import br.com.itau.caseponto.model.RegistroPonto;
import br.com.itau.caseponto.model.RegistroPontoUsuario;
import br.com.itau.caseponto.service.RegistroPontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping("/ponto/v1")
public class RegistroPontoController {

    @Autowired
    private RegistroPontoService registroPontoService;

    @PostMapping("/registros")
    public ResponseEntity<RegistroPonto> createRegistro(@Valid @RequestBody RegistroPonto registroPonto)
            throws UsuarioNotFoundException, InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        RegistroPonto createdRegistro = registroPontoService.createRegistro(registroPonto);
        createHATEOASLinks(createdRegistro);
        URI uri = new ResourceUtils<RegistroPonto>().createURI(createdRegistro);
        return ResponseEntity.created(uri).body(createdRegistro);
    }

    @GetMapping("/registros/{usuarioId}")
    public ResponseEntity<RegistroPontoUsuario> getAllRegistrosByUsuarioId(@PathVariable(value = "usuarioId") Long usuarioId) throws UsuarioNotFoundException {
        return ResponseEntity.ok().body(registroPontoService.findByUsuarioId(usuarioId));
    }

    private void createHATEOASLinks(RegistroPonto created) {
        created.add(linkTo(RegistroPontoController.class).slash("registros").slash(created.getUsuarioId()).withSelfRel());
    }
}