package br.com.itau.caseponto.model;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Table(name = "usuarios")
@Entity
public class Usuario extends RepresentationModel<Usuario> {
    public interface UsuarioCreation {
    }

    public interface UsuarioUpdate {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    private String nome;

    @NotEmpty(groups = UsuarioCreation.class)
    @Pattern(groups = {UsuarioUpdate.class, UsuarioCreation.class},
            regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$",
            message = "Informe CPF no formato: '111.222.333-44'")
    private String cpf;

    @NotEmpty(groups = UsuarioCreation.class)
    @Pattern(groups = {UsuarioUpdate.class, UsuarioCreation.class},
            regexp = "^\\d{2}-(0[1-9]|1[0-2])-\\d{4}$",
            message = "Informe data de cadastro no formato: '02-04-2020'")

    private String dataCadastro;

    public Usuario() {
    }

    public Usuario(String nome, String cpf, String dataCadastro) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataCadastro = dataCadastro;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(String dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public void update(Usuario newUser) {
        if (!StringUtils.isEmpty(newUser.getCpf()))
            this.setCpf(newUser.getCpf());

        if (!StringUtils.isEmpty(newUser.getNome()))
            this.setNome(newUser.getNome());
    }
}
