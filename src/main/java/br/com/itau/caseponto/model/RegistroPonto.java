package br.com.itau.caseponto.model;

import br.com.itau.caseponto.exception.InvalidTipoRegistroException;
import br.com.itau.caseponto.service.TipoRegistroEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

import static br.com.itau.caseponto.service.TipoRegistroEnum.SAIDA;

@Entity
@Table(name = "registros")
public class RegistroPonto extends RepresentationModel<RegistroPonto> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private Long usuarioId;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date dataHora = new Date();

    @NotEmpty
    private String tipo;

    @JsonIgnore
    private long timeBetweenRegistros;

    public RegistroPonto() {
    }

    public RegistroPonto(String tipo, Date dataHora) {
        this.tipo = tipo;
        this.dataHora = dataHora;
    }

    public RegistroPonto(Long usuarioId, String tipo) {
        this.usuarioId = usuarioId;
        this.tipo = tipo;
    }

    public RegistroPonto(Long usuarioId, String tipo, Date dataHora) {
        this.usuarioId = usuarioId;
        this.tipo = tipo;
        this.dataHora = dataHora;
    }

    public static RegistroPonto empty() {
        return new RegistroPonto(0L, "");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @JsonIgnore
    public long getTime() {
        return this.dataHora.getTime();
    }

    public long getTimeBetweenRegistros() {
        return this.timeBetweenRegistros;
    }

    public void setTimeBetweenRegistros(long millis) {
        this.timeBetweenRegistros = millis;
    }

    public void calculateTempoTrabalhado(RegistroPonto newRegistro) throws InvalidTipoRegistroException {
        if (TipoRegistroEnum.getTipoRegistro(newRegistro.getTipo()) == SAIDA) {
            long millis = Math.abs(this.getTime() - newRegistro.getTime());
            newRegistro.setTimeBetweenRegistros(millis);
        }
    }
}
