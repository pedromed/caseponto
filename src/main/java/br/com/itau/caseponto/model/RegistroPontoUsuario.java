package br.com.itau.caseponto.model;

import java.util.List;

import static java.util.concurrent.TimeUnit.*;

public class RegistroPontoUsuario {

    long tempoTrabalhadoMillis;
    String tempoTrabalhado;
    List<RegistroPonto> registros;

    public RegistroPontoUsuario(List<RegistroPonto> registros) {
        this.registros = registros;
        sumTempoTrabalhado();
    }

    public List<RegistroPonto> getRegistrosPonto() {
        return registros;
    }

    public String getTempoTrabalhado() {
        return tempoTrabalhado;
    }

    public void sumTempoTrabalhado() {
        registros.forEach(reg -> tempoTrabalhadoMillis += reg.getTimeBetweenRegistros());
        tempoTrabalhado = format(tempoTrabalhadoMillis);
    }

    private String format(long millis) {
        return String.format("%02d:%02d:%02d",
                //horas
                MILLISECONDS.toHours(millis),
                //minutos
                MILLISECONDS.toMinutes(millis) - HOURS.toMinutes(MILLISECONDS.toHours(millis)),
                //segundos
                MILLISECONDS.toSeconds(millis) - MINUTES.toSeconds(MILLISECONDS.toMinutes(millis)));
    }
}
