package br.com.itau.caseponto.service;

import br.com.itau.caseponto.exception.InvalidNewTipoRegistroException;
import br.com.itau.caseponto.exception.InvalidTipoRegistroException;
import br.com.itau.caseponto.exception.UsuarioNotFoundException;
import br.com.itau.caseponto.model.RegistroPonto;
import br.com.itau.caseponto.model.RegistroPontoUsuario;
import br.com.itau.caseponto.repository.RegistroPontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.List;

@Service
public class RegistroPontoService {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private RegistroPontoRepository registroPontoRepository;

    public RegistroPonto createRegistro(RegistroPonto newRegistro) throws UsuarioNotFoundException, InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        Long usuarioId = newRegistro.getUsuarioId();
        checkUsuario(usuarioId);
        RegistroPonto lastRegistro = getLastRegistroFromUsuarioId(usuarioId);
        lastRegistro.calculateTempoTrabalhado(newRegistro);
        checkTipoRegistro(lastRegistro.getTipo(), newRegistro.getTipo());
        return registroPontoRepository.save(newRegistro);
    }

    public RegistroPontoUsuario findByUsuarioId(Long usuarioId) throws UsuarioNotFoundException {
        checkUsuario(usuarioId);
        return new RegistroPontoUsuario(registroPontoRepository.findAllByUsuarioId(usuarioId));
    }

    void checkTipoRegistro(String lastTipoRegistro, String newTipoRegistro) throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        TipoRegistroEnum.checkNextRegistro(lastTipoRegistro, newTipoRegistro);
    }

    RegistroPonto getLastRegistro(List<RegistroPonto> registrosUsuario) {
        registrosUsuario.sort(Comparator.comparing(RegistroPonto::getDataHora));
        return registrosUsuario.get(registrosUsuario.size() - 1);
    }

    RegistroPonto getLastRegistroFromUsuarioId(Long usuarioId) {
        List<RegistroPonto> registrosUsuario = registroPontoRepository.findAllByUsuarioId(usuarioId);
        if (CollectionUtils.isEmpty(registrosUsuario)) return RegistroPonto.empty();
        return getLastRegistro(registrosUsuario);
    }

    private void checkUsuario(long usuarioId) throws UsuarioNotFoundException {
        usuarioService.findById(usuarioId);
    }
}
