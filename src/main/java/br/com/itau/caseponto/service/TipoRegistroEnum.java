package br.com.itau.caseponto.service;

import br.com.itau.caseponto.exception.InvalidNewTipoRegistroException;
import br.com.itau.caseponto.exception.InvalidTipoRegistroException;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum TipoRegistroEnum {

    PRIMEIRO, ENTRADA, SAIDA;

    private TipoRegistroEnum next;

    static {
        PRIMEIRO.next = ENTRADA;
        ENTRADA.next = SAIDA;
        SAIDA.next = ENTRADA;
    }

    public static TipoRegistroEnum getTipoRegistro(String tipo) throws InvalidTipoRegistroException {
        try {
            return TipoRegistroEnum.fromString(tipo);
        } catch (IllegalArgumentException e) {
            throw new InvalidTipoRegistroException(tipo, ENTRADA, SAIDA);
        }
    }

    public static TipoRegistroEnum fromString(String string) {
        if (StringUtils.isEmpty(string)) return PRIMEIRO;
        return Arrays.stream(TipoRegistroEnum.values())
                .filter(e -> e.name().equalsIgnoreCase(string)).findAny().orElseThrow(IllegalArgumentException::new);
    }

    public static void checkNextRegistro(String last, String next) throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        TipoRegistroEnum lastEnum = getTipoRegistro(last);
        TipoRegistroEnum nextEnum = getTipoRegistro(next);
        if (lastEnum.next != nextEnum) throw new InvalidNewTipoRegistroException(next, lastEnum.next);
    }
}
