package br.com.itau.caseponto.service;

import br.com.itau.caseponto.exception.UsuarioNotFoundException;
import br.com.itau.caseponto.model.Usuario;
import br.com.itau.caseponto.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario create(Usuario newUser) {
        return usuarioRepository.save(newUser);
    }

    public Usuario findById(Long id) throws UsuarioNotFoundException {
        return usuarioRepository
                .findById(id)
                .orElseThrow(() -> new UsuarioNotFoundException(id));
    }

    public List<Usuario> findAll() {
        return usuarioRepository.findAll();
    }

    public Usuario update(Long oldId, @Valid Usuario newUser) throws UsuarioNotFoundException {
        Usuario userToUpdate = findById(oldId);
        userToUpdate.update(newUser);
        return usuarioRepository.save(userToUpdate);
    }
}
