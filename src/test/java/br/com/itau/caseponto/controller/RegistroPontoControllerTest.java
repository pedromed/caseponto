package br.com.itau.caseponto.controller;


import br.com.itau.caseponto.exception.InvalidNewTipoRegistroException;
import br.com.itau.caseponto.model.RegistroPonto;
import br.com.itau.caseponto.model.RegistroPontoUsuario;
import br.com.itau.caseponto.service.RegistroPontoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import utils.DateUtils;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistroPontoController.class)
public class RegistroPontoControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RegistroPontoService service;

    @Test
    public void testCreateRegistroSuccess() throws Exception {
        RegistroPonto expected = new RegistroPonto();
        expected.setUsuarioId(1L);
        expected.setTipo("ENTRADA");

        String registroJson = "{\"usuarioId\": 1,\"tipo\": \"ENTRADA\"}";

        given(service.createRegistro(expected))
                .willReturn(expected);

        mvc.perform(post("/ponto/v1/registros")
                .content(registroJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.tipo", is(expected.getTipo())));

    }

    @Test
    public void testCreateRegistroEmptyTipo() throws Exception {
        String registroJson = "{\"usuarioId\": 1}";

        mvc.perform(post("/ponto/v1/registros")
                .content(registroJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateRegistroInvalidTipo() throws Exception {
        RegistroPonto expected = new RegistroPonto();
        expected.setUsuarioId(1L);
        expected.setTipo("EIJWSOIED");

        String registroJson = "{\"usuarioId\": 1,\"tipo\": \"EIJWSOIED\"}";

        given(service.createRegistro(expected))
                .willThrow(InvalidNewTipoRegistroException.class);

        mvc.perform(post("/ponto/v1/registros")
                .content(registroJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetAllRegistrosByUsuarioIdSuccess() throws Exception {
        long usuarioId = 1L;
        RegistroPonto reg1 = new RegistroPonto();
        reg1.setUsuarioId(usuarioId);
        reg1.setTipo("ENTRADA");
        reg1.setDataHora(DateUtils.of(2020, 1, 1, 9, 0));
        RegistroPonto reg2 = new RegistroPonto();
        reg2.setUsuarioId(usuarioId);
        reg2.setTipo("SAIDA");
        reg1.setDataHora(DateUtils.of(2020, 1, 1, 12, 0));

        RegistroPontoUsuario registroPontoUsuario = new RegistroPontoUsuario(Arrays.asList(reg1, reg2));

        given(service.findByUsuarioId(usuarioId))
                .willReturn(registroPontoUsuario);


        mvc.perform(get("/ponto/v1/registros/" + usuarioId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.registrosPonto", hasSize(2)))
                .andExpect(jsonPath("$.registrosPonto.[0].tipo", is("ENTRADA")))
                .andExpect(jsonPath("$.registrosPonto.[1].tipo", is("SAIDA")));
    }
}