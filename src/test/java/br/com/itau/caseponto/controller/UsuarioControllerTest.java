package br.com.itau.caseponto.controller;

import br.com.itau.caseponto.exception.UsuarioNotFoundException;
import br.com.itau.caseponto.model.Usuario;
import br.com.itau.caseponto.service.UsuarioService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UsuarioService service;

    @Test
    public void testCreateUsuarioSuccess() throws Exception {
        Usuario expected = new Usuario();
        expected.setNome("pedro");
        expected.setCpf("111.222.333-44");
        expected.setDataCadastro("02-02-2002");

        String usuarioJson = "{\"cpf\": \"111.222.333-44\",\"nome\": \"pedro\",\"dataCadastro\": \"01-01-2002\"}";

        given(service.create(expected))
                .willReturn(expected);

        mvc.perform(post("/ponto/v1/usuarios")
                .content(usuarioJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome", is(expected.getNome())));
    }

    @Test
    public void testCreateUsuarioEmptyCPF() throws Exception {
        String usuarioJson = "{\"nome\": \"pedro\",\"dataCadastro\": \"01-01-2002\"}";

        mvc.perform(post("/ponto/v1/usuarios")
                .content(usuarioJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateUsuarioInvalidCPFFormat() throws Exception {
        String usuarioJson = "{\"cpf\": \"11122233344\",\"nome\": \"pedro\",\"dataCadastro\": \"01-01-2002\"}";

        mvc.perform(post("/ponto/v1/usuarios")
                .content(usuarioJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors.[0].message", is("Informe CPF no formato: '111.222.333-44'")));
    }

    @Test
    public void testCreateUsuarioInvalidDataCadastroFormat() throws Exception {
        String usuarioJson = "{\"cpf\": \"111.222.333-44\",\"nome\": \"pedro\",\"dataCadastro\": \"01012002\"}";

        mvc.perform(post("/ponto/v1/usuarios")
                .content(usuarioJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors.[0].message", is("Informe data de cadastro no formato: '02-04-2020'")));
    }

    @Test
    public void testGetAllUsuariosSuccess() throws Exception {
        List<Usuario> expected = Arrays.asList(new Usuario("1", "1", "1"), new Usuario("2", "2", "2"));
        given(service.findAll())
                .willReturn(expected);

        mvc.perform(get("/ponto/v1/usuarios")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].nome", is("1")))
                .andExpect(jsonPath("$[1].nome", is("2")));
    }

    @Test
    public void testGetUsuarioByIdSuccess() throws Exception {
        Usuario expected = new Usuario();
        expected.setId(1L);
        expected.setNome("pedro");
        expected.setCpf("111.222.333-44");
        expected.setDataCadastro("02-02-2002");

        given(service.findById(expected.getId()))
                .willReturn(expected);

        mvc.perform(get("/ponto/v1/usuarios/" + expected.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is("pedro")));
    }

    @Test
    public void testGetUsuarioByIdNotFound() throws Exception {
        given(service.findById(Mockito.any()))
                .willThrow(UsuarioNotFoundException.class);

        mvc.perform(get("/ponto/v1/usuarios/" + 292929L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateUsuarioSuccess() throws Exception {
        Usuario old = new Usuario();
        old.setId(1L);
        old.setNome("aaa");
        old.setCpf("111.222.333-44");

        Usuario newExpected = new Usuario();
        newExpected.setNome("bbb");
        newExpected.setCpf("222.333.555-44");

        String newUsuarioJson = "{\"cpf\": \"222.333.555-44\",\"nome\": \"bbb\"}";

        given(service.findById(old.getId()))
                .willReturn(old);

        given(service.update(old.getId(), newExpected))
                .willReturn(newExpected);

        mvc.perform(put("/ponto/v1/usuarios/" + old.getId())
                .content(newUsuarioJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is(newExpected.getNome())))
                .andExpect(jsonPath("$.cpf", is(newExpected.getCpf())));
    }

    @Test
    public void testUpdateUsuarioInvalidCPFFormat() throws Exception {
        Usuario old = new Usuario();
        old.setId(1L);
        old.setNome("aaa");
        old.setCpf("111.222.333-44");

        String newUsuarioJson = "{\"cpf\": \"222.333.55544\",\"nome\": \"bbb\"}";

        given(service.findById(old.getId()))
                .willReturn(old);

        mvc.perform(put("/ponto/v1/usuarios/" + old.getId())
                .content(newUsuarioJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}