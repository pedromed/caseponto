package br.com.itau.caseponto.service;

import br.com.itau.caseponto.exception.UsuarioNotFoundException;
import br.com.itau.caseponto.model.Usuario;
import br.com.itau.caseponto.repository.UsuarioRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    @TestConfiguration
    static class UsuarioServiceTestContextConfiguration {
        @Bean
        public UsuarioService usuarioService() {
            return new UsuarioService();
        }
    }

    @Test
    public void testCreateNewUsuarioSuccess() {
        Usuario expected = new Usuario("test", "3849839483", "01/01/01");
        Mockito.when(usuarioRepository.save(expected))
                .thenReturn(expected);
        Usuario actual = usuarioService.create(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testFindUsuarioByIdSuccess() throws UsuarioNotFoundException {
        Usuario expected = new Usuario("test", "3849839483", "01/01/01");
        Mockito.when(usuarioRepository.findById(1L))
                .thenReturn(Optional.of(expected));
        Usuario actual = usuarioService.findById(1L);
        assertEquals(expected, actual);
    }

    @Test(expected = UsuarioNotFoundException.class)
    public void testFindusuarioByIdNotFound() throws UsuarioNotFoundException {
        usuarioService.findById(9829L);
    }

    @Test
    public void testFindAllUsuariosSuccess() {
        Usuario mock1 = new Usuario("u1", "3849839483", "01/01/01");
        Usuario mock2 = new Usuario("u2", "0239453029", "02/02/02");
        List<Usuario> usuariosExpected = Arrays.asList(mock1, mock2);
        Mockito.when(usuarioRepository.findAll())
                .thenReturn(usuariosExpected);
        List<Usuario> usuariosActual = usuarioService.findAll();
        assertEquals(usuariosExpected, usuariosActual);
    }

    @Test
    public void testFindAllUsuarioEmptyList() {
        Mockito.when(usuarioRepository.findAll())
                .thenReturn(new ArrayList<>());
        List<Usuario> usuariosActual = usuarioService.findAll();
        assertEquals(new ArrayList<>(), usuariosActual);
    }

    @Test
    public void testUpdateUsuarioSuccess() throws UsuarioNotFoundException {
        Usuario oldUsuario = new Usuario("old", "1111111111", "01/01/01");
        Usuario expectedUpdatedUsuario = new Usuario("new", "2222222222", "01/01/01");
        Mockito.when(usuarioRepository.findById(1L))
                .thenReturn(Optional.of(oldUsuario));
        Mockito.when(usuarioRepository.save(expectedUpdatedUsuario))
                .thenReturn(expectedUpdatedUsuario);

        Usuario actualUpdatedUsuario = usuarioService.update(1L, expectedUpdatedUsuario);
        assertEquals(expectedUpdatedUsuario, actualUpdatedUsuario);
    }

    @Test(expected = UsuarioNotFoundException.class)
    public void testUpdateUsuarioNotFound() throws UsuarioNotFoundException {
        usuarioService.update(27727L, new Usuario("new", "2222222222", "01/01/01"));
    }
}