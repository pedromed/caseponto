package br.com.itau.caseponto.service;

import br.com.itau.caseponto.exception.InvalidNewTipoRegistroException;
import br.com.itau.caseponto.exception.InvalidTipoRegistroException;
import br.com.itau.caseponto.exception.UsuarioNotFoundException;
import br.com.itau.caseponto.model.RegistroPonto;
import br.com.itau.caseponto.model.RegistroPontoUsuario;
import br.com.itau.caseponto.repository.RegistroPontoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import utils.DateUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class RegistroPontoServiceTest {

    @MockBean
    private RegistroPontoRepository registroPontoRepository;
    @Autowired
    private RegistroPontoService registroPontoService;
    @MockBean
    private UsuarioService usuarioService;

    @TestConfiguration
    static class RegistroPontoServiceTestContextConfiguration {
        @Bean
        public RegistroPontoService registroPontoService() {
            return new RegistroPontoService();
        }
    }

    @Test
    public void testCreateNewRegistroSuccess() throws Exception {
        RegistroPonto expected = new RegistroPonto(1L, "ENTRADA");
        Mockito.when(registroPontoRepository.save(expected))
                .thenReturn(expected);

        RegistroPonto actual = registroPontoService.createRegistro(expected);
        assertEquals(expected, actual);
    }

    @Test(expected = UsuarioNotFoundException.class)
    public void testCreateNewRegistroUsuarioNotFound() throws Exception {
        RegistroPonto expected = new RegistroPonto(2L, "ENTRADA");
        Mockito.when(registroPontoRepository.save(expected))
                .thenReturn(expected);
        Mockito.when(usuarioService.findById(2L))
                .thenThrow(UsuarioNotFoundException.class);
        registroPontoService.createRegistro(expected);
    }

    @Test(expected = InvalidTipoRegistroException.class)
    public void testCreateNewRegistroInvalidTipo() throws Exception {
        RegistroPonto expected = new RegistroPonto(1L, "BLABLA");
        registroPontoService.createRegistro(expected);
    }

    @Test
    public void testCreateNextRegistroSuccess() throws Exception {
        RegistroPonto newRegistro = new RegistroPonto(1L, "SAIDA");
        Mockito.when(registroPontoRepository.findAllByUsuarioId(1L))
                .thenReturn(Collections.singletonList(new RegistroPonto(1L, "ENTRADA")));

        Mockito.when(registroPontoRepository.save(newRegistro))
                .thenReturn(newRegistro);

        RegistroPonto actual = registroPontoService.createRegistro(newRegistro);
        assertEquals(newRegistro, actual);
    }

    @Test(expected = InvalidNewTipoRegistroException.class)
    public void testCreateNewRegistroEntrada_Entrada() throws Exception {
        Mockito.when(registroPontoRepository.findAllByUsuarioId(1L))
                .thenReturn(Collections.singletonList(new RegistroPonto(1L, "ENTRADA")));

        RegistroPonto newRegistro = new RegistroPonto(1L, "ENTRADA");

        Mockito.when(registroPontoRepository.save(newRegistro))
                .thenReturn(newRegistro);

        registroPontoService.createRegistro(newRegistro);
    }

    @Test(expected = InvalidNewTipoRegistroException.class)
    public void testCreateNewRegistroSaida_Saida() throws Exception {
        Mockito.when(registroPontoRepository.findAllByUsuarioId(1L))
                .thenReturn(Collections.singletonList(new RegistroPonto(1L, "SAIDA")));

        RegistroPonto newRegistro = new RegistroPonto(1L, "SAIDA");

        Mockito.when(registroPontoRepository.save(newRegistro))
                .thenReturn(newRegistro);

        registroPontoService.createRegistro(newRegistro);
    }

    @Test(expected = InvalidNewTipoRegistroException.class)
    public void testCreateNewRegistroPrimeiroRegistroSaida() throws Exception {
        Mockito.when(registroPontoRepository.findAllByUsuarioId(1L))
                .thenReturn(Collections.singletonList(RegistroPonto.empty()));

        RegistroPonto newRegistro = new RegistroPonto(1L, "SAIDA");

        Mockito.when(registroPontoRepository.save(newRegistro))
                .thenReturn(newRegistro);

        registroPontoService.createRegistro(newRegistro);
    }

    @Test
    public void testCreateCalculateTempoTrabalhadoSuccess() throws Exception {
        Date entradaDate = new DateUtils()
                .hour(8)
                .day(25)
                .month(12)
                .year(2019)
                .build();

        Date saidaDate = new DateUtils()
                .hour(12)
                .day(25)
                .month(12)
                .year(2019)
                .build();

        Mockito.when(registroPontoRepository.findAllByUsuarioId(1L))
                .thenReturn(Collections.singletonList(new RegistroPonto(1L, "ENTRADA", entradaDate)));

        RegistroPonto newRegistro = new RegistroPonto(1L, "SAIDA", saidaDate);

        Mockito.when(registroPontoRepository.save(newRegistro))
                .thenReturn(newRegistro);

        RegistroPonto saved = registroPontoService.createRegistro(newRegistro);
        assertEquals(14400000, saved.getTimeBetweenRegistros());
    }

    @Test
    public void testGetLastRegistroFromUserIdSuccess() throws Exception {
        Date entradaDate = new DateUtils()
                .hour(8)
                .day(25)
                .month(12)
                .year(2019)
                .build();

        Date saidaDate = new DateUtils()
                .hour(12)
                .day(25)
                .month(12)
                .year(2019)
                .build();

        Mockito.when(registroPontoRepository.findAllByUsuarioId(1L))
                .thenReturn(Collections.singletonList(new RegistroPonto(1L, "ENTRADA", entradaDate)));

        RegistroPonto newRegistro = new RegistroPonto(1L, "SAIDA", saidaDate);

        Mockito.when(registroPontoRepository.save(newRegistro))
                .thenReturn(newRegistro);

        RegistroPonto saved = registroPontoService.createRegistro(newRegistro);
        assertEquals(14400000, saved.getTimeBetweenRegistros());
    }

    @Test
    public void testGetLastRegistroSuccess() {
        RegistroPonto expectedLastRegistro = new RegistroPonto("", DateUtils.of(2017, 1, 29, 19, 30));
        RegistroPonto r2016 = new RegistroPonto("", DateUtils.of(2016, 1, 29, 19, 30));
        RegistroPonto r2015 = new RegistroPonto("", DateUtils.of(2015, 1, 29, 19, 30));

        Mockito.when(registroPontoRepository.findAllByUsuarioId(1L))
                .thenReturn(Arrays.asList(expectedLastRegistro, r2016, r2015));

        RegistroPonto actualLastRegistro = registroPontoService.getLastRegistroFromUsuarioId(1L);
        assertEquals(expectedLastRegistro, actualLastRegistro);
    }

    @Test
    public void testGetRegistroPontoUsuarioTempoTrabalhadoSuccess() throws Exception {
        long usuarioId = 1L;
        RegistroPonto entrada1 = new RegistroPonto(usuarioId, "ENTRADA", DateUtils.of(2020, 1, 1, 9, 0));
        RegistroPonto saida1 = new RegistroPonto(usuarioId, "SAIDA", DateUtils.of(2020, 1, 1, 12, 0));
        RegistroPonto entrada2 = new RegistroPonto(usuarioId, "ENTRADA", DateUtils.of(2020, 1, 1, 13, 0));
        RegistroPonto saida2 = new RegistroPonto(usuarioId, "SAIDA", DateUtils.of(2020, 1, 1, 18, 1));

        Mockito.when(registroPontoRepository.findAllByUsuarioId(usuarioId))
                .thenReturn(Collections.singletonList(RegistroPonto.empty()));
        registroPontoService.createRegistro(entrada1);

        Mockito.when(registroPontoRepository.findAllByUsuarioId(usuarioId))
                .thenReturn(Collections.singletonList(entrada1));
        registroPontoService.createRegistro(saida1);

        Mockito.when(registroPontoRepository.findAllByUsuarioId(usuarioId))
                .thenReturn(Arrays.asList(entrada1, saida1));
        registroPontoService.createRegistro(entrada2);

        Mockito.when(registroPontoRepository.findAllByUsuarioId(usuarioId))
                .thenReturn(Arrays.asList(entrada1, saida1, entrada2));
        registroPontoService.createRegistro(saida2);
        Mockito.when(registroPontoRepository.findAllByUsuarioId(usuarioId))
                .thenReturn(Arrays.asList(entrada1, saida1, entrada2, saida2));

        RegistroPontoUsuario registroPontoUsuario = registroPontoService.findByUsuarioId(usuarioId);
        assertEquals("08:01:00", registroPontoUsuario.getTempoTrabalhado());
    }

    @Test
    public void testGetRegistroPontoUsuarioRegistroListSuccess() throws UsuarioNotFoundException {
        long usuarioId = 1L;
        RegistroPonto entrada1 = new RegistroPonto(usuarioId, "ENTRADA");
        RegistroPonto saida1 = new RegistroPonto(usuarioId, "SAIDA");
        RegistroPonto entrada2 = new RegistroPonto(usuarioId, "ENTRADA");
        RegistroPonto saida2 = new RegistroPonto(usuarioId, "SAIDA");

        Mockito.when(registroPontoRepository.findAllByUsuarioId(usuarioId))
                .thenReturn(Arrays.asList(entrada1, saida1, entrada2, saida2));

        RegistroPontoUsuario registroPontoUsuario = registroPontoService.findByUsuarioId(usuarioId);
        assertEquals("ENTRADA", registroPontoUsuario.getRegistrosPonto().get(0).getTipo());
        assertEquals("SAIDA", registroPontoUsuario.getRegistrosPonto().get(1).getTipo());
        assertEquals("ENTRADA", registroPontoUsuario.getRegistrosPonto().get(2).getTipo());
        assertEquals("SAIDA", registroPontoUsuario.getRegistrosPonto().get(3).getTipo());
    }

    @Test(expected = InvalidNewTipoRegistroException.class)
    public void testCheckTipoRegistroDuasEntradas() throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        registroPontoService.checkTipoRegistro("ENTRADA", "ENTRADA");
    }

    @Test(expected = InvalidNewTipoRegistroException.class)
    public void testCheckTipoRegistroDuasSaidas() throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        registroPontoService.checkTipoRegistro("SAIDA", "SAIDA");
    }

    @Test(expected = InvalidNewTipoRegistroException.class)
    public void testCheckTipoRegistraPrimeiroRegistroSaida() throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        registroPontoService.checkTipoRegistro("", "SAIDA");
    }

    @Test
    public void testCheckTipoRegistroEntradaSaida() throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        registroPontoService.checkTipoRegistro("ENTRADA", "SAIDA");
    }

    @Test
    public void testCheckTipoRegistraSaidaEntrada() throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        registroPontoService.checkTipoRegistro("SAIDA", "ENTRADA");
    }

    @Test
    public void testCheckTipoRegistraPrimeiroRegistroEntrada() throws InvalidTipoRegistroException, InvalidNewTipoRegistroException {
        registroPontoService.checkTipoRegistro("", "ENTRADA");
    }
}