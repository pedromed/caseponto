package br.com.itau.caseponto.model;

import br.com.itau.caseponto.exception.InvalidTipoRegistroException;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RegistroPontoUsuarioTest {

    @Test
    public void testSumTempoTrabalhado() throws InvalidTipoRegistroException {
        List<RegistroPonto> registros = new ArrayList<>();
        LocalDateTime entrada1 = LocalDateTime.of(2015, Month.JULY, 29, 9, 0, 0);
        LocalDateTime saida1 = LocalDateTime.of(2015, Month.JULY, 29, 12, 0, 0);

        LocalDateTime entrada2 = LocalDateTime.of(2015, Month.JULY, 29, 13, 0, 0);
        LocalDateTime saida2 = LocalDateTime.of(2015, Month.JULY, 29, 18, 0, 0);

        RegistroPonto regEntrada1 = new RegistroPonto("ENTRADA", Date.from(entrada1.atZone(ZoneId.systemDefault()).toInstant()));
        RegistroPonto regSaida1 = new RegistroPonto("SAIDA", Date.from(saida1.atZone(ZoneId.systemDefault()).toInstant()));

        RegistroPonto regEntrada2 = new RegistroPonto("ENTRADA", Date.from(entrada2.atZone(ZoneId.systemDefault()).toInstant()));
        RegistroPonto regSaida2 = new RegistroPonto("SAIDA", Date.from(saida2.atZone(ZoneId.systemDefault()).toInstant()));

        regEntrada1.calculateTempoTrabalhado(regSaida1);
        regEntrada2.calculateTempoTrabalhado(regSaida2);

        registros.add(regEntrada1);
        registros.add(regSaida1);
        registros.add(regEntrada2);
        registros.add(regSaida2);

        RegistroPontoUsuario registroUsuario = new RegistroPontoUsuario(registros);

        assertEquals("08:00:00", registroUsuario.getTempoTrabalhado());

    }
}