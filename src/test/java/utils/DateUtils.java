package utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;

    public static Date of(int year, int month, int day, int hour, int minute) {
        return Date.from(LocalDateTime
                .of(year, month, day, hour, minute, 0)
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public Date build() {
        return Date.from(LocalDateTime
                .of(year, month, day, hour, minute, second)
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public DateUtils year(int year) {
        this.year = year;
        return this;
    }

    public DateUtils month(int month) {
        this.month = month;
        return this;
    }

    public DateUtils day(int day) {
        this.day = day;
        return this;
    }

    public DateUtils hour(int hour) {
        this.hour = hour;
        return this;
    }

    public DateUtils minute(int minute) {
        this.minute = minute;
        return this;
    }

    public DateUtils second(int second) {
        this.second = second;
        return this;
    }
}
