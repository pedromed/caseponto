## API Registro Ponto

#### Usuario
[Cadastra novo usuário](#post-pontov1usuarios) <br/>
[Busca todos usuários](#get-pontov1usuarios) <br/>
[Busca usuário por ID](#get-pontov1usuariosid) <br/>
[Atualiza usuário](#put-pontov1usuariosid) <br/>

#### Registro Ponto
[Cadastra novo registro de ponto](#post-pontov1registros) <br/>
[Busca registros de um usuario](#post-pontov1registros) <br/>
___
### POST /ponto/v1/usuarios
**Exemplo requisicao**
```
{
    "nome": "usuario 1",
    "cpf": "11.222.333-44",
    "dataCadastro": "01-01-2002"
}
```
**Exemplo resposta**
```
{
    "id": 1,
    "nome": "usuario 1",
     "cpf": "11.222.333-44",
    "dataCadastro": "01-01-2002"
    "_links": {
        "self": {
            "href": "http://example_url/ponto/v1/1"
        }
    }
}
```
---
### GET /ponto/v1/usuarios
**Exemplo resposta**
```
[
    {
        "id": 1,
        "nome": "usuario 1",
        "cpf": "111.111.222-11",
        "dataCadastro": "01-01-2002",
        "links": []
    },
    {
        "id": 2,
        "nome": "usuario 2",
        "cpf": "222.333.222-11",
        "dataCadastro": "01-01-2002",
        "links": []
    }
]
```
---
### GET /ponto/v1/usuarios/{id}
**Parametros**

|          Nome | Obrigatorio |  Tipo   | Descricao                                                                       |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|     `id` | sim | long  | Numero de identificacao unico do usuario |                                                                    |

**Exemplo resposta**
```
{
    "id": 1,
    "nome": "usuario 1",
    "cpf": "11.222.333-44",
    "dataCadastro": "01-01-2002"
}
```
---
### PUT /ponto/v1/usuarios/{id}
**Parametros**

|          Nome | Obrigatorio |  Tipo   | Descricao                                                                       |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|     `id` | sim | long  | Numero de identificacao unico do usuario |                                                                    |

**Exemplo requisicao**
```
{
    "nome": "novo nome",
    "cpf": "11.222.333-44",
}
```
**Exemplo resposta**
```
{
    "id": 1,
    "nome": "novo nome",
     "cpf": "11.222.333-44",
    "dataCadastro": "01-01-2002"
    "_links": {
        "self": {
             href": "http://example_url/ponto/v1/usuarios/1"
        }
    }
}
```
---
### POST /ponto/v1/registros
**Exemplo requisicao**
```
{
    "usuarioId": 1,
    "tipo": "ENTRADA"
}
```
**Exemplo resposta**
```
{
    "id": 23,
    "usuarioId": 1,
    "dataHora": "01-01-2020 22:00:00",
    "tipo": "ENTRADA",
    "_links": {
        "self": {
            href": "http://example_url/ponto/v1/registros/1"
        }
    }
}
```
---
### GET /ponto/v1/registros/{id}
**Exemplo resposta**
```
{
    "tempoTrabalhado": "00:03:37",
    "registrosPonto": [
        {
            "id": 2,
            "usuarioId": 1,
            "dataHora": "01-01-2020 23:42:58",
            "tipo": "ENTRADA",
            "links": []
        },
        {
            "id": 3,
            "usuarioId": 1,
            "dataHora": "01-01-2020 23:46:36",
            "tipo": "SAIDA",
            "links": []
        }
    ]
}
```